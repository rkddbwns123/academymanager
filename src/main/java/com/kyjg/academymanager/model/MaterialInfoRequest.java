package com.kyjg.academymanager.model;

import com.kyjg.academymanager.enums.Classroom;
import com.kyjg.academymanager.enums.Manager;
import com.kyjg.academymanager.enums.Material;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MaterialInfoRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Material material;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Manager manager;
    @NotNull
    @Min(value = 1)
    @Max(value = 1000)
    private Integer useMaterialAmount;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Classroom classroom;
    @NotNull
    private LocalDate purchaseDate;
}
