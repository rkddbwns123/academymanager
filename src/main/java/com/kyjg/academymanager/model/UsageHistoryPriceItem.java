package com.kyjg.academymanager.model;

import com.kyjg.academymanager.entity.UsageHistory;
import com.kyjg.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageHistoryPriceItem {
    private Long historyId;
    private String materialName;
    private String userName;
    private String isAcademyStudent;
    private LocalDate useDate;
    private Integer useTime;
    private Integer howManyUser;
    private Integer usePrice;

    private UsageHistoryPriceItem(UsageHistoryPriceItemBuilder builder) {
        this.historyId = builder.historyId;
        this.materialName = builder.materialName;
        this.userName = builder.userName;
        this.isAcademyStudent = builder.isAcademyStudent;
        this.useDate = builder.useDate;
        this.useTime = builder.useTime;
        this.howManyUser = builder.howManyUser;
        this.usePrice = builder.usePrice;

    }

    public static class UsageHistoryPriceItemBuilder implements CommonModelBuilder<UsageHistoryPriceItem> {

        private final Long historyId;
        private final String materialName;
        private final String userName;
        private final String isAcademyStudent;
        private final LocalDate useDate;
        private final Integer useTime;
        private final Integer howManyUser;
        private final Integer usePrice;

        public UsageHistoryPriceItemBuilder(UsageHistory history) {
            this.historyId = history.getId();
            this.materialName = history.getMaterialInfo().getMaterial().getMaterialName();
            this.userName = history.getUserName();
            this.isAcademyStudent = history.getIsAcademyStudent() ? "학원 수강생" : "외부 이용자";
            this.useDate = history.getUseDate();
            this.useTime = history.getUseTime();
            this.howManyUser = history.getHowManyUser();
            this.usePrice = history.getUseTime() * history.getHowManyUser() * history.getMaterialInfo().getMaterial().getUseAmountHour();
        }

        @Override
        public UsageHistoryPriceItem build() {
            return new UsageHistoryPriceItem(this);
        }
    }
}
