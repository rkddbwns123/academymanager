package com.kyjg.academymanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UsageHistoryRequest {
    @NotNull
    @Length(min = 1, max = 20)
    private String userName;
    @NotNull
    private Boolean isAcademyStudent;
    @NotNull
    private Integer useTime;
    @NotNull
    private Integer howManyUser;
}
