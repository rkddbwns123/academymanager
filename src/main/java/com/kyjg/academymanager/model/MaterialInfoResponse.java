package com.kyjg.academymanager.model;

import com.kyjg.academymanager.entity.MaterialInfo;
import com.kyjg.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MaterialInfoResponse {
    private Long materialId;
    private String materialName;
    private LocalDate purchaseDate;
    private String managerName;
    private String managerPhone;
    private String classroomLocation;
    private LocalDate replacementPeriod;

    private MaterialInfoResponse(MaterialInfoResponseBuilder builder) {
        this.materialId = builder.materialId;
        this.materialName = builder.materialName;
        this.purchaseDate = builder.purchaseDate;
        this.managerName = builder.managerName;
        this.managerPhone = builder.managerPhone;
        this.classroomLocation = builder.classroomLocation;
        this.replacementPeriod = builder.replacementPeriod;
    }

    public static class MaterialInfoResponseBuilder implements CommonModelBuilder<MaterialInfoResponse> {

        private final Long materialId;
        private final String materialName;
        private final LocalDate purchaseDate;
        private final String managerName;
        private final String managerPhone;
        private final String classroomLocation;
        private final LocalDate replacementPeriod;

        public MaterialInfoResponseBuilder(MaterialInfo info) {
            this.materialId = info.getId();
            this.materialName = info.getMaterial().getMaterialName();
            this.purchaseDate = info.getPurchaseDate();
            this.managerName = info.getManager().getMangerName();
            this.managerPhone = info.getManager().getManagerPhone();
            this.classroomLocation = info.getClassroom().getClassroomLocation() + " " + info.getClassroom().getClassroomName();
            this.replacementPeriod = info.getPurchaseDate().plusYears(info.getMaterial().getReplacementPeriod());
        }

        @Override
        public MaterialInfoResponse build() {
            return new MaterialInfoResponse(this);
        }
    }
}
