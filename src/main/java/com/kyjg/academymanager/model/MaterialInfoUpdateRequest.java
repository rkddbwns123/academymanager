package com.kyjg.academymanager.model;

import com.kyjg.academymanager.enums.Manager;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MaterialInfoUpdateRequest {

    @Enumerated(value = EnumType.STRING)
    @NotNull
    private Manager manager;
}
