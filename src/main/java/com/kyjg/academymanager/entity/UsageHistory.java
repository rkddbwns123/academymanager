package com.kyjg.academymanager.entity;

import com.kyjg.academymanager.interfaces.CommonModelBuilder;
import com.kyjg.academymanager.model.UsageHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "materialInfoId", nullable = false)
    private MaterialInfo materialInfo;
    @Column(nullable = false, length = 20)
    private String userName;
    @Column(nullable = false)
    private Boolean isAcademyStudent;
    @Column(nullable = false)
    private LocalDate useDate;
    @Column(nullable = false)
    private Integer useTime;
    @Column(nullable = false)
    private Integer howManyUser;

    private UsageHistory(UsageHistoryBuilder builder) {
        this.materialInfo = builder.materialInfo;
        this.userName = builder.userName;
        this.isAcademyStudent = builder.isAcademyStudent;
        this.useDate = builder.useDate;
        this.useTime = builder.useTime;
        this.howManyUser = builder.howManyUser;

    }

    public static class UsageHistoryBuilder implements CommonModelBuilder<UsageHistory> {

        private final MaterialInfo materialInfo;
        private final String userName;
        private final Boolean isAcademyStudent;
        private final LocalDate useDate;
        private final Integer useTime;
        private final Integer howManyUser;

        public UsageHistoryBuilder(MaterialInfo materialInfo, UsageHistoryRequest request) {
            this.materialInfo = materialInfo;
            this.userName = request.getUserName();
            this.isAcademyStudent = request.getIsAcademyStudent();
            this.useDate = LocalDate.now();
            this.useTime = request.getUseTime();
            this.howManyUser = request.getHowManyUser();
        }

        @Override
        public UsageHistory build() {
            return new UsageHistory(this);
        }
    }


}
