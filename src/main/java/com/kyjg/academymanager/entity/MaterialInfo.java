package com.kyjg.academymanager.entity;

import com.kyjg.academymanager.enums.Classroom;
import com.kyjg.academymanager.enums.Manager;
import com.kyjg.academymanager.enums.Material;
import com.kyjg.academymanager.interfaces.CommonModelBuilder;
import com.kyjg.academymanager.model.MaterialInfoRequest;
import com.kyjg.academymanager.model.MaterialInfoUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MaterialInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    private Material material;
    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private Manager manager;
    @Column(nullable = false)
    private LocalDate purchaseDate;
    @Column(nullable = false)
    private Integer useMaterialAmount;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Classroom classroom;

    public void putMaterialManagerInfo(MaterialInfoUpdateRequest request) {
        this.manager = request.getManager();
    }

    private MaterialInfo(MaterialInfoBuilder builder) {
        this.material = builder.material;
        this.manager = builder.manager;
        this.purchaseDate = builder.purchaseDate;
        this.useMaterialAmount = builder.useMaterialAmount;
        this.classroom = builder.classroom;

    }

    public static class MaterialInfoBuilder implements CommonModelBuilder<MaterialInfo> {

        private final Material material;
        private final Manager manager;
        private final LocalDate purchaseDate;
        private final Integer useMaterialAmount;
        private final Classroom classroom;

        public MaterialInfoBuilder(MaterialInfoRequest request) {
            this.material = request.getMaterial();
            this.manager = request.getManager();
            this.purchaseDate = request.getPurchaseDate();
            this.useMaterialAmount = request.getUseMaterialAmount();
            this.classroom = request.getClassroom();
        }

        @Override
        public MaterialInfo build() {
            return new MaterialInfo(this);
        }
    }
}
