package com.kyjg.academymanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
