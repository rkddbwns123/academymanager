package com.kyjg.academymanager.controller;

import com.kyjg.academymanager.entity.MaterialInfo;
import com.kyjg.academymanager.model.CommonResult;
import com.kyjg.academymanager.model.ListResult;
import com.kyjg.academymanager.model.UsageHistoryPriceItem;
import com.kyjg.academymanager.model.UsageHistoryRequest;
import com.kyjg.academymanager.service.MaterialInfoService;
import com.kyjg.academymanager.service.ResponseService;
import com.kyjg.academymanager.service.UsageHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-history")
public class UsageHistoryController {
    private final UsageHistoryService usageHistoryService;
    private final MaterialInfoService materialInfoService;
    @PostMapping("/data/material-info-id/{materialInfoId}")
    public CommonResult setUsageHistory(@PathVariable long materialInfoId, @RequestBody @Valid UsageHistoryRequest request) {
        MaterialInfo materialInfo = materialInfoService.getData(materialInfoId);
        usageHistoryService.setUsageHistory(materialInfo, request);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("/price")
    public ListResult<UsageHistoryPriceItem> getPrice() {
        return ResponseService.getListResult(usageHistoryService.getPrice(), true);
    }
}
