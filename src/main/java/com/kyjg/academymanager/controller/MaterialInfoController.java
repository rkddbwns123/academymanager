package com.kyjg.academymanager.controller;

import com.kyjg.academymanager.model.*;
import com.kyjg.academymanager.service.MaterialInfoService;
import com.kyjg.academymanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/material-info")
public class MaterialInfoController {
    private final MaterialInfoService materialInfoService;
    @PostMapping("/data")
    public CommonResult setMaterialInfo(@RequestBody @Valid MaterialInfoRequest request) {
        materialInfoService.setMaterialInfo(request);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("/info/material-id/{materialId}")
    public SingleResult<MaterialInfoResponse> getMaterialInfo(@PathVariable long materialId) {
        return ResponseService.getSingleResult(materialInfoService.getMaterialInfo(materialId));
    }
    @PutMapping("/manager/{id}")
    public CommonResult putMaterialManagerInfo(@PathVariable long id, MaterialInfoUpdateRequest request) {
        materialInfoService.putMaterialManagerInfo(id, request);

        return ResponseService.getSuccessResult();
    }
    @DeleteMapping("/info/{id}")
    public CommonResult delMaterialInfo(@PathVariable long id) {
        materialInfoService.delMaterialInfo(id);

        return ResponseService.getSuccessResult();
    }
}
