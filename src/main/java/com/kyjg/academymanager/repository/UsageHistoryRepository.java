package com.kyjg.academymanager.repository;

import com.kyjg.academymanager.entity.UsageHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsageHistoryRepository extends JpaRepository<UsageHistory, Long> {
    List<UsageHistory> findAllByIsAcademyStudentOrderByIdDesc(Boolean isAcademyStudent);
}
