package com.kyjg.academymanager.repository;

import com.kyjg.academymanager.entity.MaterialInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterialInfoRepository extends JpaRepository<MaterialInfo, Long> {
}
