package com.kyjg.academymanager.service;

import com.kyjg.academymanager.entity.MaterialInfo;
import com.kyjg.academymanager.entity.UsageHistory;
import com.kyjg.academymanager.model.ListResult;
import com.kyjg.academymanager.model.UsageHistoryPriceItem;
import com.kyjg.academymanager.model.UsageHistoryRequest;
import com.kyjg.academymanager.repository.UsageHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageHistoryService {
    private final UsageHistoryRepository usageHistoryRepository;

    public void setUsageHistory(MaterialInfo materialInfo, UsageHistoryRequest request) {
        UsageHistory addData = new UsageHistory.UsageHistoryBuilder(materialInfo, request).build();

        usageHistoryRepository.save(addData);
    }
    public ListResult<UsageHistoryPriceItem> getPrice() {
        List<UsageHistory> originList = usageHistoryRepository.findAllByIsAcademyStudentOrderByIdDesc(false);

        List<UsageHistoryPriceItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new UsageHistoryPriceItem.UsageHistoryPriceItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
