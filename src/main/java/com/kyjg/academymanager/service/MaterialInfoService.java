package com.kyjg.academymanager.service;

import com.kyjg.academymanager.entity.MaterialInfo;
import com.kyjg.academymanager.exception.CMissingDataException;
import com.kyjg.academymanager.model.MaterialInfoRequest;
import com.kyjg.academymanager.model.MaterialInfoResponse;
import com.kyjg.academymanager.model.MaterialInfoUpdateRequest;
import com.kyjg.academymanager.repository.MaterialInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MaterialInfoService {
    private final MaterialInfoRepository materialInfoRepository;

    public void setMaterialInfo(MaterialInfoRequest request) {
        MaterialInfo addData = new MaterialInfo.MaterialInfoBuilder(request).build();

        materialInfoRepository.save(addData);
    }
    public MaterialInfo getData(long id) {
        return materialInfoRepository.findById(id).orElseThrow();
    }

    public MaterialInfoResponse getMaterialInfo(long id) {
        MaterialInfo originData = materialInfoRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new MaterialInfoResponse.MaterialInfoResponseBuilder(originData).build();
    }
    public void putMaterialManagerInfo(long id, MaterialInfoUpdateRequest request) {
        MaterialInfo originData = materialInfoRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putMaterialManagerInfo(request);

        materialInfoRepository.save(originData);
    }

    public void delMaterialInfo(long id) {
        materialInfoRepository.deleteById(id);
    }
}
