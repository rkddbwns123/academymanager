package com.kyjg.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Manager {
    MAIN_MANAGER("김현수", "010-0000-0000"),
    SUB_MANAGER("강은지", "010-0000-0001"),
    OTHER_MANAGER("서이수", "010-0000-0002");

    private final String mangerName;
    private final String managerPhone;
}
