package com.kyjg.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Classroom {
    ROOM_1("1강의실", "4층"),
    ROOM_2("2강의실", "4층"),
    ROOM_3("3강의실" , "5층");

    private final String classroomName;
    private final String classroomLocation;
}
