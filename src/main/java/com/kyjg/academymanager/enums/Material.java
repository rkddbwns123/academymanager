package com.kyjg.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Material {
    COMPUTER("컴퓨터", 5, 3000),
    KEYBOARD("키보드", 2, 500),
    MONITOR("모니터", 4, 1000);

    private final String materialName;
    private final int replacementPeriod;
    private final int useAmountHour;
}
